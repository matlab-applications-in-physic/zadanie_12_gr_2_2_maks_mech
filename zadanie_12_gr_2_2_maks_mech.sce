clc
clear

//constants all in SI units
G = 80000000000 //80 GPa
r = 0.002 //2 mm
R = 0.02*2.54 //2 in
m = 0.010 //10 g
g = 9.81 //m/s
tau = 2 //s time constant
bet = 1/(2*tau)
frequency = 50:200
N = 2:50; 

// main  function calculating amplitude & resonance
for i = 1:49
   k = (G*r^4)/(4*N(i)*R^3);
   amplitude(i, 1) = (g/sqrt((sqrt(k/m)^2 - frequency(1)^2)^2 + 4*bet^2*frequency(1)^2));
   for j = 2:size(frequency)(2)
      amplitude(i, j) = (g/sqrt((sqrt(k/m)^2 - frequency(j)^2)^2 + 4*bet^2*frequency(j)^2));
   end
   resonance(i) = max(amplitude(i, :)); 
end

// plotting amplitude(frequency)
subplot(211);
plot(frequency, amplitude(1:49, :));
xlabel('frequency [Hz]')
ylabel('amplitude [m]')

// plotting amplitude(N) where resonance is found
subplot(212);
plot(N, resonance);
xlabel('number')
ylabel('amplitude [m]')

// saving to pdf
xs2pdf(0, 'resonance.pdf'); 

// saving data to dat file
file0 = mopen('resonance_analysis_results.dat', 'w+') 
for i = 1:49
    mfprintf(file0, "%i%s%f\n" , N(i), ';', resonance(i));
end
mclose(file0); 
